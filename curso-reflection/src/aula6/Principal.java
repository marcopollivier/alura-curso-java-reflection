package aula6;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Principal {

	public static void main(String[] args) throws Exception {
		TesteErros obj = new TesteErros();
		
		Class<?> clazz = obj.getClass();
		
		try {
			Method m = clazz.getDeclaredMethod("metodo", String.class);
			m.invoke(obj, "string");
		} catch (InvocationTargetException e) {
			e.getTargetException().printStackTrace();
		}
		
	}
	
}
