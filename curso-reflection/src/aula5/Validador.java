package aula5;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Validador {

	public static boolean validarObjeto(Object obj) { 
		boolean resultado = true;
		
		Class<?> clazz = obj.getClass();
		
		try {
			for (Method method : clazz.getMethods()) {
				if (method.getName().startsWith("validar") 
						&& method.getReturnType() == boolean.class
						&& method.getParameterTypes().length == 0) {
					
					Boolean retorno = (Boolean) method.invoke(obj);
					resultado = resultado && retorno.booleanValue();
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		
		return resultado;
	}
	
	public static List<Object> retornaMetodos(Object obj) throws Exception { 
		Class<?> clazz = obj.getClass();
		List<Object> retorno = new ArrayList<>();
		
		for (Method method : clazz.getMethods()) {
			if (method.getName().startsWith("test") 
					&& method.getReturnType() == void.class) {
				retorno.add(method.invoke(obj));
			}
		}
		
		
		return retorno;
	}
	
}
