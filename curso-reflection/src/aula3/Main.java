package aula3;

import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		
		MapaDeClasse m = new MapaDeClasse();
		m.load("classes.prop");
		
		List l = m.getInstancia(List.class);
		InterfaceExemplo i = m.getInstancia(InterfaceExemplo.class, "interface");
		
		System.out.println(l.getClass());
		System.out.println(i.getClass());
	}
	
}
