package aula1;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ValidadorNulo {

	public List<String> getAtributos (Object obj) {
		
		try {
			List<String> lista = new ArrayList<>();
			Class<?> classe = obj.getClass();
			for (Field field : classe.getFields()) {
				Object value = field.get(obj);
				if (value == null) {
					lista.add(field.getName());
				}
			}
			
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
}
