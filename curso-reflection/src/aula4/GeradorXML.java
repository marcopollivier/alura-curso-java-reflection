package aula4;

import java.lang.reflect.Field;

public class GeradorXML {

	public static String getXML(Object obj) throws Exception {
		StringBuilder sb = new StringBuilder();
		
		Class<?> clazz = obj.getClass();
		
		sb.append("<"+ clazz.getSimpleName() +">");
		sb.append("\n");
		
		for (Field field : clazz.getDeclaredFields()) {
			sb.append("<"+ field.getName() +">");
			sb.append("\n");
			
			field.setAccessible(true); //pode ser bloqueado pelo security manager	
			sb.append(field.get(obj))	;
			
			sb.append("</"+ field.getName() +">");
			sb.append("\n");
		}
		
		sb.append("</"+ clazz.getSimpleName() +">");
		sb.append("\n");
		
		return sb.toString();
	}
	
}
