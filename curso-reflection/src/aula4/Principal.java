package aula4;

public class Principal {

	public static void main(String[] args) throws Exception {
		
		Usuario u = new Usuario();
		u.setLogin("guerra");
		u.setSenha("senhadoguerra");
		u.setEmail("guerra@guerra.com");
		u.setPapel("professor");
		u.setAtivo(true);
		
		String xml = GeradorXML.getXML(u);
		
		System.out.println(xml);

	}

}
